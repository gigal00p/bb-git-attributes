#!/usr/bin/env bb
(require '[clojure.test :as t])
(require 'clj-gattr.main-test)

(def test-results
  (t/run-tests 'clj-gattr.main-test))

(def failures-and-errors
  (let [{:keys [:fail :error]} test-results]
    (+ fail error)))

(System/exit failures-and-errors)
