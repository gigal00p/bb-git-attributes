(ns clj-gattr.main
  (:require [clj-yaml.core :as yaml]
            [clojure.string :as str]))

(defn string-contains-digit?
  "Returns true if input-string contains a digit, false otherwise"
  [input-string]
  {:pre [(string? input-string)]}
  (let [array-of-characters (seq (char-array (str/trim input-string)))
        retval (some #(Character/isDigit %) array-of-characters)]
    (if retval true false)))

(defn string-contains-char?
  "Returns true if input-string contains searched-char, false otherwise"
  [input-string searched-char]
  {:pre [(string? input-string)
         (string? searched-char)]}
  (try
    (cond
      (and (> (count input-string) 0) (str/blank? searched-char)) false
      :else (.contains input-string searched-char))
    (catch Exception e (str "Caught exception: " (.getMessage e)))))

(defn generate-out-string
  "Handles regex creation for 3 cases: letter, dot and digit"
  [s]
  {:pre [(string? s) (= (count s) 1) (not (str/blank? s))]}
  (if (nil? (re-matches #"^[a-zA-Z0-9\.]+$" s))
    (throw (IllegalArgumentException. "Invalid character in the input string. Only alphanumerics and `.` allowed"))
    (cond
      (string-contains-char? s ".") s
      (string-contains-digit? s) s
      :else (str "[" (.toLowerCase s) (.toUpperCase s) "]"))))

(defn process-single-extension
  "Generates gitattributes entry for extension-string"
  [extension-string]
  (let [vector-of-strings (-> extension-string
                              (str/trim)       ; trim any whitespace
                              .toLowerCase     ; normalize case
                              (str/split #"")) ; split string to vector of one-element strings
        prefix-string "*."
        postfix-string " filter=lfs diff=lfs merge=lfs -text"
        retval (map #(generate-out-string %) vector-of-strings)]
    (str prefix-string (str/join retval) postfix-string)))

(defn write-lazy-seq-to-file!
  "Write a lazy seq to a file-name"
  [seq file-name]
  (->>
   seq
   (interpose \newline)
   (apply str)
   (spit file-name)))

(defn make-attributes
  "Process config file (input-map) and generates list of strings for each extension"
  [input-map]
  (let [extensions-kinds (->> input-map :extensions_kinds keys)
        retval (for [ext (sort extensions-kinds)
                     :let [section-header (str "\n# " (str/capitalize (name ext)))
                           processed-elements (map #(process-single-extension %) (->> input-map :extensions_kinds ext sort))]]
                 (conj processed-elements section-header))]
    (flatten retval)))

(defn main
  [file_extensions]
  (let [input-string (slurp file_extensions)
        all-extensions (yaml/parse-string input-string)
        preambule (str/join \newline ["# Repository: https://gitlab.com/gigal00p/bb-git-attributes"
                                      "# Feel free to make MR with new file extensions that should be tracked in LFS"])
        attributes (make-attributes all-extensions)]
    (write-lazy-seq-to-file! (conj attributes preambule) ".gitattributes")))

(main "file_extensions.yml")

(comment
  (require 'clojure.test 'clj-gattr.main-test)
  (clojure.test/run-tests 'clj-gattr.main-test)
  )
