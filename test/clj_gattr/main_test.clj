(ns clj-gattr.main-test
  (:require [clj-gattr.main :as cg]
            [clojure.test :refer [deftest is testing]]))

(deftest string-contains-digit-test
  (testing "Test if string contains digit"
    [(is (false? (cg/string-contains-digit? "")))
     (is (false? (cg/string-contains-digit? "mpv")))
     (is (false? (cg/string-contains-digit? "lzo")))
     (is (cg/string-contains-digit? "tar.bz2"))
     (is (cg/string-contains-digit? "7z"))
     (is (cg/string-contains-digit? "3gp"))]))

(deftest string-contains-char-test
  (testing "Test if string contains a character"
    [(is (false? (cg/string-contains-char? "mpv" "a")))
     (is (false? (cg/string-contains-char? "ogv" "")))
     (is (false? (cg/string-contains-char? "" "z")))
     (is (false? (cg/string-contains-char? "z" "")))
     (is (cg/string-contains-char? "mp3" "3"))
     (is (cg/string-contains-char? "tar.bz2" "."))
     (is (cg/string-contains-char? "7z" "z"))
     (is (cg/string-contains-char? "3gp" "p"))]))

(deftest generate-out-string-test
  (testing "Test creation of the pattern"
    [(is (= (cg/generate-out-string "m") "[mM]"))
     (is (= (cg/generate-out-string "U") "[uU]"))
     (is (= (cg/generate-out-string "3") "3"))
     (is (= (cg/generate-out-string ".") "."))]))

(deftest process-single-extension-test
  (testing "Test creation of the final string pattern for extension"
    [(is (= (cg/process-single-extension "mp3") "*.[mM][pP]3 filter=lfs diff=lfs merge=lfs -text"))
     (is (= (cg/process-single-extension "7z") "*.7[zZ] filter=lfs diff=lfs merge=lfs -text"))
     (is (= (cg/process-single-extension "tar.bz2") "*.[tT][aA][rR].[bB][zZ]2 filter=lfs diff=lfs merge=lfs -text"))]))
