# clj_gatr.clj - generate .gitattributes using babashka

This is a simple [babashka][babashka-url] script to generate
[.gitattributes][gitattributes-url] file with file extensions that should be
tracket by [git-lfs][git-lfs-url].

Rationale: there are certain file extensions that shouldn't be commited directly
into `git`. These are mostly binary files. See `file_extensions.yml`. Feel free
to make MR with new file extensions that should be tracked in LFS.

## Usage

To generate `.gitattributes` file run `bb src/clj_gattr/main.clj` at the command
line after clonnig this repo. File `file_extensions.yml` must be present in the
same directory.

You can also download latest `.gitattributes` from this repository latest CI/CD
pipeline job. Simply drill through latest pipelnie/job and download the file.

[babashka-url]: https://github.com/babashka/babashka
[git-lfs-url]: https://git-lfs.github.com/
[gitattributes-url]: https://git-scm.com/docs/gitattributes
